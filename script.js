
const navList = document.querySelector('.tabs');
const navItems = document.querySelectorAll('.tabs-title');
const contentList = document.querySelector('.tabs-content');
const contentListItems = contentList.querySelectorAll('li');

let prevMenuItem = null;
let prevText = null;

for (let i = 0; i < navItems.length; i++) {
    navItems[i].dataset.dataNumber = i;
    contentListItems[i].dataset.dataNumber = i;
}

navList.addEventListener('click', showText);
/*открытие первого списка меню при загрузке страницы*/
navItems[0].click();

function showText(event) {
    /*подсветка выбранного пункта меню */
    if (prevMenuItem) {
        prevMenuItem.classList.remove('active');
    }
    navItems.forEach((item) => {
        if (item.classList.contains('active')) {
            item.classList.remove('active');
        }
    });

    let activeMenuItem = event.target;
    activeMenuItem.classList.add('active');

    prevMenuItem = activeMenuItem;

    /*показ текста*/

    let tabNumber = activeMenuItem.dataset.dataNumber;

    if (prevText) {
        prevText.style.display = 'none';
    }
    contentListItems.forEach(item => {
        if (item.dataset.dataNumber === tabNumber) {
            item.style.display = 'block';
            prevText = item;
        }
    })
}



